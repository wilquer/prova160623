#!/bin/bash


verificar_usuario() {
    read -p "Digite o nome do usuário: " nome_usuario
    if id "$nome_usuario" >/dev/null 2>&1; then
        echo "O usuário $nome_usuario existe."
    else
        echo "O usuário $nome_usuario não existe."
    fi
}


verificar_login() {
    read -p "Digite o nome do usuário: " nome_usuario
    if who | grep -w "$nome_usuario" >/dev/null; then
        echo "O usuário $nome_usuario está logado."
    else
        echo "O usuário $nome_usuario não está logado."
    fi
}

listar_arquivos() {
    read -p "Digite o nome do usuário: " nome_usuario
    if id "$nome_usuario" >/dev/null 2>&1; then
        echo "Arquivos da pasta home do usuário $nome_usuario:"
        ls /home/"$nome_usuario"
    else
        echo "O usuário $nome_usuario não existe."
    fi
}

while true; do
    echo "Menu:"
    echo "1. Verificar se o usuário existe"
    echo "2. Verificar se o usuário está logado na máquina"
    echo "3. Listar os arquivos da pasta home do usuário"
    echo "4. Sair"

    read -p "Digite o número da opção desejada (ou 'd' para sair): " opcao

    case $opcao in
        1)
            verificar_usuario
            ;;
         2)
            verificar_login
            ;;
         3)
            listar_arquivos
            ;;
         4)
            echo "Saindo..."
            exit 0
            ;;
         d)
            echo "Saindo ihhuuulllll ..."
            exit 0
            ;;
    esac

    echo
done

