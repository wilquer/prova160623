#!/bin/bash

# Nome do arquivo de lista
lista_arquivo="lista.txt"

# Nome do arquivo de resultado
resultado_arquivo="resultado.txt"

# Verifica se o arquivo de lista existe
if [ ! -f "$lista_arquivo" ]; then
  echo "O arquivo de lista '$lista_arquivo' não existe."
  exit 1
fi

# Remove o arquivo de resultado se já existir
if [ -f "$resultado_arquivo" ]; then
  rm "$resultado_arquivo"
fi

# Loop para processar cada arquivo da lista
while IFS= read -r arquivo; do
  # Verifica se o arquivo atual existe
  if [ ! -f "$arquivo" ]; then
    echo "O arquivo '$arquivo' não existe."
    continue
  fi

  # Calcula o hash do arquivo (usando md5sum)
  hash=$(md5sum "$arquivo" | cut -d' ' -f1)

  # vai salvar o nome do arquivo e o hash no reultado 
  echo "$hash $arquivo" >> "$resultado_arquivo"

  echo "Hash do arquivo '$arquivo' calculado e salvo."
done < "$lista_arquivo"

echo "concluídoooo. Os resultados foram salvos em '$resultado_arquivo'."

