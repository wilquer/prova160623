#!/bin/bash

jogador_hp=$1
dragao_hp=$2
dragon_name="Hungarian Horntail"

echo "Seja bem vindo ao jogo do dragão"
sleep 1
echo
echo "Tente se vencer terás uma conquista e 10 pontos para a casa" 
sleep 1
echo
echo "Então nobre cavaleiro faça sua escolha"
sleep 1
echo
echo "venha a luta ou fuja como um covarde"
sleep 1
echo
echo "Dragão: ASFBSAFBLKJDSBGKJDSNBFÇKJDSBFLKJDSLGF" Ps:isso ai foi o rugido do dragão, soltando fogo e etc kkkkkk
sleep 1
echo
while true; do
    echo "Escolha uma opção:"
    echo "1. Atacar"
    echo "2. Fugir"
    echo "3. Info"
    read -p "Opção: " option

    case $option in
        1)
            echo "Você ataca o $dragon_name e causa 100 pontos de dano."
            dragao_hp=$((dragao_hp - 100))
            echo "O $dragon_name te causa 10 pontos de dano."
            player_hp=$((player_hp - 10))
            ;;
        2)
            escape_chance=$((RANDOM % 2))
            if [ $escape_chance -eq 0 ]; then
                echo "Você conseguiu fugir do $dragon_name!"
                break
            else
                echo "Você tenta fugir, mas é atingido por uma baforada do $dragon_name!"
                player_hp=0
                break
            fi
            ;;
        3)
            echo "Vida restante:"
            echo "Você: $player_hp pontos de vida"
            echo "$dragon_name: $dragon_hp pontos de vida"
            ;;
        *)
            echo "Opção inválida. Por favor, escolha novamente."
            ;;
    esac

    if [ $player_hp -le 200 ]; then
        echo "Você foi derrotado pelo $dragon_name!"
        break
    fi

    if [ $dragon_hp -le 500 ]; then
        echo "Parabéns! Você derrotou o $dragon_name!"
        break
    fi

    echo ""
done

cowsay -f dragon "Fim de jogo"

